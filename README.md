Secure Header Wrapper for [Bottlepy](https://bottlepy.org/docs/dev/#) Applications, forked from https://github.com/twaldear/flask-secure-headers and refactored for the Bottlepy framework.

## Installation
No __pip install__ available, clone this repository and then  install the extension with

```bash
$ python3 setup.py install
```

If you are not an administrator:

```bash
$ python3 setup.py build
```

and then link the build directory in a directory included in your PYTHONPATH.


## Add to a bottlepy webapp

Prior your endpoints, 

```python
from bottle_secure_headers.core import Secure_Headers                                                                 
sh = Secure_Headers()                                                                                                 
### CSP                                                                                                               
## Content Security Policy (CSP)                                                                                      
## Tell the browser where it can load various types of resource from                                                  
sh.rewrite({'CSP':{'default-src':['self']}})   
```



