from setuptools import setup


setup(
  name = 'bottle-secure-headers',
  packages = ['bottle_secure_headers'],
  include_package_data = True,
  version = '0.1',
  description = 'Secure Header Wrapper for Bottle Applications',
  long_description = ('Security headers for a bottle application. '
                      'Refactored from flask-secure-headers '
                      ),
  license='MIT',
  author = '',
  author_email = '',
  url = 'https://...bottle-secure-headers',
  download_url = '',
  keywords = ['bottle', 'security', 'header'],
  install_requires = ['bottle'],
  test_suite="nose.collector",
  tests_require = ['nose'],
  classifiers=[
    'Development Status :: 4 - Beta',
    'Framework :: Bottle',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Software Development :: Libraries :: Python Modules',
  ]
)
